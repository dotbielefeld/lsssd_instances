# Instances for the Liner Shipping Single Service Design Problem (LSSSD) with Service Levels

This repository contains instances for the LSSSD with Serivice Levels. The instances have been created using services from the 2014 COSCO network, which had schedules publicly available on the COSCO website. We combine this with [LINERLIB](https://github.com/blof/LINERLIB) data regarding vessel properties, port distances, and demand information. We release this data into the public domain.

Our data is intended to be used with the LINERLIB, which we do not distribute here. Please see the link above for the current version. We performed our work with the LINERLIB version 1.1 (commit 350c3b1).

Source code for solving the mathematical model is available on request by other researchers.

